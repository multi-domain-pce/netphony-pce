package es.tid.pce.parentPCE;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.tid.ospf.ospfv2.lsa.InterASTEv2LSA;
import es.tid.ospf.ospfv2.lsa.tlv.LinkTLV;
import es.tid.pce.pcep.objects.Notification;
import es.tid.pce.pcep.objects.tlvs.ITAdvertisementTLV;
import es.tid.pce.pcep.objects.tlvs.OSPFTE_LSA_TLV;
import es.tid.tedb.ITMDTEDB;
import es.tid.tedb.MDTEDB;

/**
 * Receives notifications with topology updates and maintains the multidomain topology
 *
 * @author ogondio
 */
public class MultiDomainTopologyUpdaterThread extends Thread {
    /**
     * The Logger
     */
    private Logger log;

    /**
     * Queue with the Updates to process
     */
    private LinkedBlockingQueue<MultiDomainUpdate> multiDomainUpdateQueue;

    // Multi-domain TEDB
    private MDTEDB multiDomainTEDB;

    //IT Capable Multi-domain TEDB
    private ITMDTEDB ITmultiDomainTEDB;

    //List of the received LSAs
    private LinkedList<InterASTEv2LSA> interASTEv2LSAList;

    public MultiDomainTopologyUpdaterThread(LinkedBlockingQueue<MultiDomainUpdate> multiDomainUpdateQueue, MDTEDB multiDomainTEDB) {
        log = LoggerFactory.getLogger("MultiDomainTologyUpdater");
        this.multiDomainUpdateQueue = multiDomainUpdateQueue;
        this.multiDomainTEDB = multiDomainTEDB;
        interASTEv2LSAList = new LinkedList<InterASTEv2LSA>();
    }

    public MultiDomainTopologyUpdaterThread(LinkedBlockingQueue<MultiDomainUpdate> multiDomainUpdateQueue, ITMDTEDB ITmultiDomainTEDB) {
        log = LoggerFactory.getLogger("MultiDomainTologyUpdater");
        this.multiDomainUpdateQueue = multiDomainUpdateQueue;
        this.ITmultiDomainTEDB = ITmultiDomainTEDB;
    }

    public void run() {
        log.info("Starting Multidomain Topology Upadater Thread");
        MultiDomainUpdate multiDomainUpdate;
        Notification notif;
        while (true) {
            try {
                multiDomainUpdate = multiDomainUpdateQueue.take();
                notif = multiDomainUpdate.getNotif();
                log.debug("Processing Notification to update topology");


                //notif.getNotifyList();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    public String printLSAList() {
        StringBuffer sb = new StringBuffer(20 + interASTEv2LSAList.size() * 100);
        sb.append(interASTEv2LSAList.size());
        sb.append(" LSAs\r\n");
        for (int i = 0; i < interASTEv2LSAList.size(); ++i) {
            sb.append(i);
            sb.append("--> ");
            sb.append(interASTEv2LSAList.get(i).toString());
            sb.append("\r\n");
            sb.append("------------------------\r\n");
            sb.append("\r\n");
        }
        return sb.toString();
    }

    public String printLSAShortList() {
        StringBuffer sb = new StringBuffer(20 + interASTEv2LSAList.size() * 100);
        sb.append(interASTEv2LSAList.size());
        sb.append(" LSAs\r\n");
        for (int i = 0; i < interASTEv2LSAList.size(); ++i) {
            sb.append(i);
            sb.append("--> ");
            sb.append(interASTEv2LSAList.get(i).printShort());
            sb.append("\r\n");
            sb.append("------------------------\r\n");
            sb.append("\r\n");
        }
        return sb.toString();
    }


    public int sizeLSAList() {
        return interASTEv2LSAList.size();
    }

}
