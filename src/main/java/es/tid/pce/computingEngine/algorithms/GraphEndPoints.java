package es.tid.pce.computingEngine.algorithms;

/**
 * @author b.mvas
 */

import es.tid.pce.pcep.constructs.EndPoint;
import es.tid.pce.pcep.constructs.EndPointAndRestrictions;
import es.tid.pce.pcep.constructs.P2PEndpoints;
import es.tid.pce.pcep.objects.EndPoints;
import es.tid.pce.pcep.objects.EndPointsIPv4;
import es.tid.pce.pcep.objects.GeneralizedEndPoints;
import es.tid.pce.pcep.objects.ObjectParameters;

public class GraphEndPoints {
    private GraphEndPoint source;
    private GraphEndPoint destination;

    public GraphEndPoints(EndPoints EP) {

        source = new GraphEndPoint();
        destination = new GraphEndPoint();

//		Object source_router_id_addr = null;
//		Object dest_router_id_addr = null;

        if (EP.getOT() == ObjectParameters.PCEP_OBJECT_TYPE_ENDPOINTS_IPV4) {
            EndPointsIPv4 ep = (EndPointsIPv4) EP;
            source.setVertex(ep.getSourceIP());
            destination.setVertex(ep.getDestIP());
        } else if (EP.getOT() == ObjectParameters.PCEP_OBJECT_TYPE_ENDPOINTS_IPV6) {
            //FIXME complete this part
        }

        if (EP.getOT() == ObjectParameters.PCEP_OBJECT_TYPE_GENERALIZED_ENDPOINTS) {
            GeneralizedEndPoints gep = (GeneralizedEndPoints) EP;

        }
    }

    /*
     * GETTERS AND SETTERS
     */
    public GraphEndPoint getSource() {
        return source;
    }

    public void setSource(GraphEndPoint source) {
        this.source = source;
    }

    public GraphEndPoint getDestination() {
        return destination;
    }

    public void setDestination(GraphEndPoint destination) {
        this.destination = destination;
    }
}
