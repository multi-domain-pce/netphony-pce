package es.tid.pce.computingEngine.algorithms;

import java.net.Inet4Address;
import java.util.List;

import es.tid.tedb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

import es.tid.of.DataPathID;
import es.tid.pce.computingEngine.ComputingRequest;
import es.tid.pce.computingEngine.ComputingResponse;
import es.tid.pce.pcep.constructs.EndPoint;
import es.tid.pce.pcep.constructs.EndPointAndRestrictions;
import es.tid.pce.pcep.constructs.Path;
import es.tid.pce.pcep.constructs.Request;
import es.tid.pce.pcep.constructs.Response;
import es.tid.pce.pcep.objects.EndPoints;
import es.tid.pce.pcep.objects.EndPointsIPv4;
import es.tid.pce.pcep.objects.ExplicitRouteObject;
import es.tid.pce.pcep.objects.GeneralizedEndPoints;
import es.tid.pce.pcep.objects.Metric;
import es.tid.pce.pcep.objects.Monitoring;
import es.tid.pce.pcep.objects.NoPath;
import es.tid.pce.pcep.objects.subobjects.SREROSubobject;
import es.tid.pce.pcep.objects.ObjectParameters;
import es.tid.pce.pcep.objects.RequestParameters;
import es.tid.pce.pcep.objects.tlvs.NoPathTLV;
import es.tid.rsvp.objects.subobjects.DataPathIDEROSubobject;
import es.tid.rsvp.objects.subobjects.IPv4prefixEROSubobject;
import es.tid.rsvp.objects.subobjects.UnnumberIfIDEROSubobject;
import es.tid.rsvp.objects.subobjects.UnnumberedDataPathIDEROSubobject;

public class DefaultSinglePathComputing implements ComputingAlgorithm {

    private SimpleDirectedWeightedGraph<Object, IntraDomainEdge> networkGraph;
    private Logger log = LoggerFactory.getLogger("PCEServer");
    private ComputingRequest pathReq;
    private TEDB ted;

    public DefaultSinglePathComputing(ComputingRequest pathReq, TEDB ted) {
        this.ted = ted;
        try {
            log.info("TED is:" + ted.getClass());
            if (ted.getClass().equals(SimpleTEDB.class)) {
                this.networkGraph = ((SimpleTEDB) ted).getDuplicatedNetworkGraph();
            } else if (ted.getClass().equals(VSPTSimpleTEDB.class)) {
                this.networkGraph = ((VSPTSimpleTEDB) ted).getDuplicatedNetworkGraph();
            } else if (ted.getClass().equals(MDTEDB.class)) {
                //this.networkGraph= ((MDTEDB)ted).getDuplicatedNetworkGraph();
                this.networkGraph = null;
            } else if (ted.getClass().equals(SimpleITTEDB.class)) {
                this.networkGraph = ((SimpleITTEDB) ted).getDuplicatedNetworkGraph();
            }

            this.pathReq = pathReq;
        } catch (Exception e) {
            this.pathReq = pathReq;
            this.networkGraph = null;
        }

    }

    public ComputingResponse call() {
        long tiempoini = System.nanoTime();
        ComputingResponse m_resp = new ComputingResponse();
        Request req = pathReq.getRequestList().get(0);
        long reqId = req.getRequestParameters().getRequestID();
        log.info("Processing Single Path Computing Request id: " + reqId);


        //Start creating the response
        Response response = new Response();
        RequestParameters rp = new RequestParameters();
        rp.setRequestID(reqId);
        response.setRequestParameters(rp);
        if (this.networkGraph == null) {
            log.info("No network Graph");
            NoPath noPath = new NoPath();
            noPath.setNatureOfIssue(ObjectParameters.NOPATH_NOPATH_SAT_CONSTRAINTS);
            NoPathTLV noPathTLV = new NoPathTLV();
            noPath.setNoPathTLV(noPathTLV);
            response.setNoPath(noPath);
            m_resp.addResponse(response);
            return m_resp;
        }


        EndPoints EP = req.getEndPoints();
        Object source_router_id_addr = null;
        Object dest_router_id_addr = null;
        Object router_xro = null;
        long source_port = 0;
        long destination_port = 0;
        DataPathID xro = new DataPathID();


        if (EP.getOT() == ObjectParameters.PCEP_OBJECT_TYPE_ENDPOINTS_IPV4) {
            EndPointsIPv4 ep = (EndPointsIPv4) req.getEndPoints();
            source_router_id_addr = ep.getSourceIP();
            dest_router_id_addr = ep.getDestIP();
        } else if (EP.getOT() == ObjectParameters.PCEP_OBJECT_TYPE_ENDPOINTS_IPV6) {
            log.info("ENDPOINTS IPv6 not supported");
        }

        if (EP.getOT() == ObjectParameters.PCEP_OBJECT_TYPE_GENERALIZED_ENDPOINTS) {
            GeneralizedEndPoints gep = (GeneralizedEndPoints) req.getEndPoints();
        }

        log.info("Algorithm->  Source:: " + source_router_id_addr + " Destination:: " + dest_router_id_addr);
        log.info("Check if we have source and destination in our TED");


        //Case XRO is not null
        if (req.getXro() != null) {
            xro.setDataPathID(req.getXro().getXROSubobjectList().getFirst().toString());
            log.info("Algorithm.getXro ::" + xro);
            if (networkGraph.containsVertex(xro)) {
                log.info("Delete node in graph:: " + xro);
                networkGraph.removeVertex(xro);
            }
        }

        if (!((networkGraph.containsVertex(source_router_id_addr)) && (networkGraph.containsVertex(dest_router_id_addr)))) {
            log.warn("DefaultSinglePathComputing:: Source or destination are NOT in the TED");
            NoPath noPath = new NoPath();
            noPath.setNatureOfIssue(ObjectParameters.NOPATH_NOPATH_SAT_CONSTRAINTS);
            NoPathTLV noPathTLV = new NoPathTLV();
            if (!((networkGraph.containsVertex(source_router_id_addr)))) {
                log.warn("Unknown source");
                noPathTLV.setUnknownSource(true);
            }
            if (!((networkGraph.containsVertex(dest_router_id_addr)))) {
                log.warn("Unknown destination");
                noPathTLV.setUnknownDestination(true);
            }

            noPath.setNoPathTLV(noPathTLV);
            response.setNoPath(noPath);
            m_resp.addResponse(response);
            return m_resp;
        }

        log.debug("Computing path");
        //long tiempoini =System.nanoTime();
        DijkstraShortestPath<Object, IntraDomainEdge> dsp = new DijkstraShortestPath<Object, IntraDomainEdge>(networkGraph, source_router_id_addr, dest_router_id_addr);
        GraphPath<Object, IntraDomainEdge> gp = dsp.getPath();

        log.debug("Creating response");
        if (gp == null) {
            log.warn("DefaultSinglePathComputing:: No Path Found");
            NoPath noPath = new NoPath();
            noPath.setNatureOfIssue(ObjectParameters.NOPATH_NOPATH_SAT_CONSTRAINTS);
            response.setNoPath(noPath);
            m_resp.addResponse(response);
            return m_resp;
        }

        // Code ERO Object
        m_resp.addResponse(response);
        Path path = new Path();
        ExplicitRouteObject ero = new ExplicitRouteObject();
        List<IntraDomainEdge> edge_list = gp.getEdgeList();

        // Add first hop in the ERO Object in there is an interface
        if (source_port != 0) {
            log.warn("Source SID:" + edge_list.get(0).getSrc_sid());
            if (edge_list.get(0).getSource() instanceof Inet4Address) {
                UnnumberIfIDEROSubobject eroso = new UnnumberIfIDEROSubobject();
                eroso.setRouterID((Inet4Address) edge_list.get(0).getSource());
                eroso.setInterfaceID(source_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(0).getSource() instanceof DataPathID) {
                UnnumberedDataPathIDEROSubobject eroso = new UnnumberedDataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(0).getSource());
                eroso.setInterfaceID(source_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else {
                log.info("Edge instance error");
            }
        }

        // Add intermediate hops
        int i;
        for (i = 0; i < edge_list.size(); i++) {
            log.warn("Source SID:" + edge_list.get(i).getSrc_sid());
            log.warn("DST SID:" + edge_list.get(i).getDst_sid());
            if (edge_list.get(i).getDst_sid() != 0) {
                SREROSubobject eroso = new SREROSubobject();
                eroso.setSID(edge_list.get(i).getDst_sid());
                eroso.setLoosehop(false);
                eroso.setST((byte) 1);
                eroso.setMflag(true);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(i).getSource() instanceof Inet4Address) {
                UnnumberIfIDEROSubobject eroso = new UnnumberIfIDEROSubobject();
                eroso.setRouterID((Inet4Address) edge_list.get(i).getSource());
                eroso.setInterfaceID(edge_list.get(i).getSrc_if_id());
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(i).getSource() instanceof DataPathID) {
                UnnumberedDataPathIDEROSubobject eroso = new UnnumberedDataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(i).getSource());
                eroso.setInterfaceID(edge_list.get(i).getSrc_if_id());
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else {
                log.info("Edge instance error");
            }
        }
        // Add last hop in the ERO Object
        log.info("jm dspc destination_port: " + destination_port);
        if (destination_port != 0) {
            if (edge_list.get(edge_list.size() - 1).getDst_sid() != 0) {
                log.info("SR so skipping last entry");
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof Inet4Address) {
                log.info("jm defoultsingle ultima interfaz ip" + edge_list.get(edge_list.size() - 1));
                UnnumberIfIDEROSubobject eroso = new UnnumberIfIDEROSubobject();
                eroso.setRouterID((Inet4Address) edge_list.get(edge_list.size() - 1).getTarget());
                eroso.setInterfaceID(destination_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof DataPathID) {
                log.info("jm defoultsingle ultima interfaz dpid" + edge_list.get(edge_list.size() - 1));
                UnnumberedDataPathIDEROSubobject eroso = new UnnumberedDataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(edge_list.size() - 1).getTarget());
                eroso.setInterfaceID(destination_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            }
        } else {
            if (edge_list.get(edge_list.size() - 1).getDst_sid() != 0) {
                log.info("SR so skipping last entry");
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof Inet4Address) {
                IPv4prefixEROSubobject eroso = new IPv4prefixEROSubobject();
                eroso.setIpv4address((Inet4Address) edge_list.get(edge_list.size() - 1).getTarget());
                eroso.setPrefix(32);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof DataPathID) {
                DataPathIDEROSubobject eroso = new DataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(edge_list.size() - 1).getTarget());
                ero.addEROSubobject(eroso);
            } else {
                log.info("Edge instance error");
            }
        }

        log.info("Algorithm.ero :: " + ero.toString());
        path.setEro(ero);
        log.info("Algorithm.path:: " + path.toString());

        if (req.getMetricList().size() != 0) {
            Metric metric = new Metric();
            metric.setMetricType(req.getMetricList().get(0).getMetricType());
            log.debug("Number of hops " + edge_list.size());
            float metricValue = (float) edge_list.size();
            metric.setMetricValue(metricValue);
            path.getMetricList().add(metric);
        }
        response.addPath(path);
        long tiempofin = System.nanoTime();
        long tiempotot = tiempofin - tiempoini;
        log.info("Ha tardado " + tiempotot + " nanosegundos");
        Monitoring monitoring = pathReq.getMonitoring();
        if (monitoring != null) {
            if (monitoring.isProcessingTimeBit()) {

            }
        }
        return m_resp;
    }

    @Override
    public AlgorithmReservation getReserv() {
        // TODO Auto-generated method stub
        return null;
    }


}
