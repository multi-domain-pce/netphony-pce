package es.tid.pce.computingEngine.algorithms;

import es.tid.of.DataPathID;
import es.tid.pce.computingEngine.ComputingRequest;
import es.tid.pce.computingEngine.ComputingResponse;
import es.tid.pce.multiPCE.MultiPCESession;
import es.tid.pce.pcep.constructs.PCEPIntiatedLSP;
import es.tid.pce.pcep.constructs.Path;
import es.tid.pce.pcep.constructs.Request;
import es.tid.pce.pcep.constructs.Response;
import es.tid.pce.pcep.messages.PCEPInitiate;
import es.tid.pce.pcep.objects.*;
import es.tid.pce.pcep.objects.subobjects.SREROSubobject;
import es.tid.pce.pcep.objects.tlvs.NoPathTLV;
import es.tid.pce.pcep.objects.tlvs.PathBindingTLV;
import es.tid.pce.pcep.objects.tlvs.PathSetupTLV;
import es.tid.pce.pcep.objects.tlvs.SymbolicPathNameTLV;
import es.tid.pce.server.DomainPCESession;
import es.tid.pce.server.management.PCEManagementSession;
import es.tid.rsvp.objects.subobjects.DataPathIDEROSubobject;
import es.tid.rsvp.objects.subobjects.IPv4prefixEROSubobject;
import es.tid.rsvp.objects.subobjects.UnnumberIfIDEROSubobject;
import es.tid.rsvp.objects.subobjects.UnnumberedDataPathIDEROSubobject;
import es.tid.tedb.*;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Inet4Address;
import java.net.InetSocketAddress;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class DefaultVSPTPathComputing implements ComputingAlgorithm {

    private SimpleDirectedWeightedGraph<Object, IntraDomainEdge> networkGraph;
    private Logger log = LoggerFactory.getLogger("PCEServer");
    private ComputingRequest pathReq;
    private TEDB ted;
    private static int[] stitching_list = {9000,9001,9002,9003,9004,9005,9006,9007,9008,9009};
    private static int stich_index = 0;
    public DefaultVSPTPathComputing(ComputingRequest pathReq, TEDB ted) {
        this.ted = ted;
        try {
            log.info("TED is:" + ted.getClass());
            if (ted.getClass().equals(SimpleTEDB.class)) {
                this.networkGraph = ((SimpleTEDB) ted).getDuplicatedNetworkGraph();
            } else if (ted.getClass().equals(VSPTSimpleTEDB.class)) {
                this.networkGraph = ((VSPTSimpleTEDB) ted).getDuplicatedNetworkGraph();
            } else if (ted.getClass().equals(MDTEDB.class)) {
                //this.networkGraph= ((MDTEDB)ted).getDuplicatedNetworkGraph();
                this.networkGraph = null;
            } else if (ted.getClass().equals(SimpleITTEDB.class)) {
                this.networkGraph = ((SimpleITTEDB) ted).getDuplicatedNetworkGraph();
            }

            this.pathReq = pathReq;
        } catch (Exception e) {
            this.pathReq = pathReq;
            this.networkGraph = null;
        }

    }

    public ComputingResponse call() {
        long tiempoini = System.nanoTime();
        ComputingResponse m_resp = new ComputingResponse();
        Request req = pathReq.getRequestList().get(0);
        long reqId = req.getRequestParameters().getRequestID();
        log.info("Processing VSPT Path Computing Request id: " + reqId);


        //Start creating the response
        Response response = new Response();
        RequestParameters rp = new RequestParameters();
        rp.setRequestID(reqId);
        response.setRequestParameters(rp);
        if (this.networkGraph == null) {
            log.info("No network Graph");
            NoPath noPath = new NoPath();
            noPath.setNatureOfIssue(ObjectParameters.NOPATH_NOPATH_SAT_CONSTRAINTS);
            NoPathTLV noPathTLV = new NoPathTLV();
            noPath.setNoPathTLV(noPathTLV);
            response.setNoPath(noPath);
            m_resp.addResponse(response);
            return m_resp;
        }


        EndPoints EP = req.getEndPoints();
        Inet4Address source_router_id_addr = null;
        Inet4Address dest_router_id_addr = null;
        Inet4Address stiching_router_id_addr = null;

        Object router_xro = null;
        long source_port = 0;
        long destination_port = 0;
        DataPathID xro = new DataPathID();


        if (EP.getOT() == ObjectParameters.PCEP_OBJECT_TYPE_ENDPOINTS_IPV4) {
            EndPointsIPv4 ep = (EndPointsIPv4) req.getEndPoints();
            source_router_id_addr = ep.getSourceIP();
            dest_router_id_addr = ep.getDestIP();
        }


        log.info("Algorithm->  Source:: " + source_router_id_addr + " Destination:: " + dest_router_id_addr);
        log.info("Check if we have source and destination in our TED");

        if (!((networkGraph.containsVertex(dest_router_id_addr)))) {
            log.warn("DefaultVSPTPathComputing:: destination is NOT in the TED");
            NoPath noPath = new NoPath();
            noPath.setNatureOfIssue(ObjectParameters.NOPATH_NOPATH_SAT_CONSTRAINTS);
            NoPathTLV noPathTLV = new NoPathTLV();
            if (!((networkGraph.containsVertex(dest_router_id_addr)))) {
                log.warn("Unknown destination");
                noPathTLV.setUnknownDestination(true);
            }

            noPath.setNoPathTLV(noPathTLV);
            response.setNoPath(noPath);
            m_resp.addResponse(response);
            return m_resp;
        }

        // TODO update source to border router.
        VSPTSimpleTEDB vsptSimpleTEDB = ((VSPTSimpleTEDB) ted);
        Inet4Address source_domain = vsptSimpleTEDB.getReachabilityManager().getDomain(source_router_id_addr);
        LinkedList<InterDomainEdge> domainList = vsptSimpleTEDB.getInterDomainLinks();
        Iterator<InterDomainEdge> it_domain = domainList.iterator();
        log.info("searching for SourceDomain :" + source_domain);
        while(it_domain.hasNext()){
            InterDomainEdge tmp_edge= it_domain.next();
            log.info("Edge:" + tmp_edge);
            log.info("Source Domain"+  tmp_edge.domain_src_router.toString());
            log.info("Dst Domain"+  tmp_edge.domain_dst_router.toString());
            if(tmp_edge.domain_dst_router.equals(source_domain)){
                log.info("Source Domain found");
                log.info("Source router"+  tmp_edge.src_router_id.toString());
                log.info("Dst router"+  tmp_edge.dst_router_id.toString());

                source_router_id_addr = (Inet4Address) tmp_edge.src_router_id;
                stiching_router_id_addr = (Inet4Address) tmp_edge.dst_router_id;
                log.info("New Source is :" + source_router_id_addr);
            }

        }

        log.debug("Computing path");
        //long tiempoini =System.nanoTime();
        if(networkGraph.containsVertex(source_router_id_addr) && networkGraph.containsVertex(dest_router_id_addr)){
            log.info("Source and Dst are now in pair");
            log.info(((VSPTSimpleTEDB)ted).printBaseTopology());
        }else{
            log.info("We fucked up");
        }
        DijkstraShortestPath<Object, IntraDomainEdge> dsp = new DijkstraShortestPath<Object, IntraDomainEdge>(networkGraph, source_router_id_addr, dest_router_id_addr);
        GraphPath<Object, IntraDomainEdge> gp = dsp.getPath();

        log.debug("Creating response");
        if (gp == null) {
            log.warn("DefaultVSPTPathComputing:: No Path Found");
            NoPath noPath = new NoPath();
            noPath.setNatureOfIssue(ObjectParameters.NOPATH_NOPATH_SAT_CONSTRAINTS);
            response.setNoPath(noPath);
            m_resp.addResponse(response);
            return m_resp;
        }

        // Code ERO Object
        m_resp.addResponse(response);
        Path path = new Path();
        ExplicitRouteObject ero = new ExplicitRouteObject();
        List<IntraDomainEdge> edge_list = gp.getEdgeList();
        log.warn("Edge List" + edge_list);



        // Add first hop in the ERO Object in there is an interface
        if (source_port != 0) {
            log.warn("Source SID:" + edge_list.get(0).getSrc_sid());
            if (edge_list.get(0).getSource() instanceof Inet4Address) {
                UnnumberIfIDEROSubobject eroso = new UnnumberIfIDEROSubobject();
                eroso.setRouterID((Inet4Address) edge_list.get(0).getSource());
                eroso.setInterfaceID(source_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(0).getSource() instanceof DataPathID) {
                UnnumberedDataPathIDEROSubobject eroso = new UnnumberedDataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(0).getSource());
                eroso.setInterfaceID(source_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else {
                log.info("Edge instance error");
            }
        }

        // Add intermediate hops
        int i;
        for (i = 0; i < edge_list.size(); i++) {
            log.warn("Source SID:" + edge_list.get(i).getSrc_sid());
            log.warn("DST SID:" + edge_list.get(i).getDst_sid());
            if (edge_list.get(i).getDst_sid() != 0) {
                SREROSubobject eroso = new SREROSubobject();
                eroso.setSID(edge_list.get(i).getDst_sid());
                eroso.setLoosehop(false);
                eroso.setST((byte) 0);
                eroso.setMflag(true);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(i).getSource() instanceof Inet4Address) {
                UnnumberIfIDEROSubobject eroso = new UnnumberIfIDEROSubobject();
                eroso.setRouterID((Inet4Address) edge_list.get(i).getSource());
                eroso.setInterfaceID(edge_list.get(i).getSrc_if_id());
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(i).getSource() instanceof DataPathID) {
                UnnumberedDataPathIDEROSubobject eroso = new UnnumberedDataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(i).getSource());
                eroso.setInterfaceID(edge_list.get(i).getSrc_if_id());
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else {
                log.info("Edge instance error");
            }
        }
        // Add last hop in the ERO Object
        log.info("jm dspc destination_port: " + destination_port);
        if (destination_port != 0) {
            if(edge_list.size() == 1){
                log.info("ENd node classs" +  gp.getEndVertex().getClass());

            }
            else if (edge_list.get(edge_list.size() - 1).getDst_sid() != 0) {
                log.info("SR so skipping last entry");
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof Inet4Address) {
                log.info("jm defoultsingle ultima interfaz ip" + edge_list.get(edge_list.size() - 1));
                UnnumberIfIDEROSubobject eroso = new UnnumberIfIDEROSubobject();
                eroso.setRouterID((Inet4Address) edge_list.get(edge_list.size() - 1).getTarget());
                eroso.setInterfaceID(destination_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof DataPathID) {
                log.info("jm defoultsingle ultima interfaz dpid" + edge_list.get(edge_list.size() - 1));
                UnnumberedDataPathIDEROSubobject eroso = new UnnumberedDataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(edge_list.size() - 1).getTarget());
                eroso.setInterfaceID(destination_port);
                eroso.setLoosehop(false);
                ero.addEROSubobject(eroso);
            }
        } else {
            if (edge_list.size() == 0 || edge_list.get(edge_list.size() - 1).getDst_sid() != 0) {
                log.info("SR so skipping last entry");
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof Inet4Address) {
                IPv4prefixEROSubobject eroso = new IPv4prefixEROSubobject();
                eroso.setIpv4address((Inet4Address) edge_list.get(edge_list.size() - 1).getTarget());
                eroso.setPrefix(32);
                ero.addEROSubobject(eroso);
            } else if (edge_list.get(edge_list.size() - 1).getTarget() instanceof DataPathID) {
                DataPathIDEROSubobject eroso = new DataPathIDEROSubobject();
                eroso.setDataPath((DataPathID) edge_list.get(edge_list.size() - 1).getTarget());
                ero.addEROSubobject(eroso);
            } else {
                log.info("Edge instance error");
            }
        }

        int stitching_sid = stitching_list[stich_index];
        stich_index = stich_index +1 % 9;

        ExplicitRouteObject ero_back = new ExplicitRouteObject();
        SREROSubobject eroso = new SREROSubobject();
        eroso.setSID(stitching_sid);
        eroso.setLoosehop(false);
        eroso.setST((byte) 0);
        eroso.setMflag(true);
        ero_back.addEROSubobject(eroso);

        path.setEro(ero_back);
        log.info("Algorithm.ero :: " + ero.toString());
        //path.setEro(ero);
        log.info("Algorithm.path:: " + path.toString());

        if (req.getMetricList().size() != 0) {
            Metric metric = new Metric();
            metric.setMetricType(req.getMetricList().get(0).getMetricType());
            log.debug("Number of hops " + edge_list.size());
            float metricValue = (float) edge_list.size();
            metric.setMetricValue(metricValue);
            path.getMetricList().add(metric);
        }
        response.addPath(path);
        long tiempofin = System.nanoTime();
        long tiempotot = tiempofin - tiempoini;
        log.info("Ha tardado " + tiempotot + " nanosegundos");
        Monitoring monitoring = pathReq.getMonitoring();
        if (monitoring != null) {
            if (monitoring.isProcessingTimeBit()) {

            }
        }

        PCEPIntiatedLSP pcepIntiatedLSPList = new PCEPIntiatedLSP();
        LSP lsp = new LSP();
        SRP rsp = new SRP();
        PathSetupTLV pstlv = new PathSetupTLV();
        pstlv.setPST(PathSetupTLV.SR);
        rsp.setPathSetupTLV(pstlv);

        SymbolicPathNameTLV symPath = new SymbolicPathNameTLV();
        PathBindingTLV bindPath = new PathBindingTLV();
        bindPath.setSID(stitching_sid);
        String symPathName = "initate";
        symPath.setSymbolicPathNameID(symPathName.getBytes());
        lsp.setSymbolicPathNameTLV_tlv(symPath);
        lsp.setPathBindingTLV_tlv(bindPath);

        PCEPInitiate pceInit = new PCEPInitiate();
        pcepIntiatedLSPList.setEro(ero);
        pcepIntiatedLSPList.setRsp(rsp);
        pcepIntiatedLSPList.setLsp(lsp);
        pceInit.getPcepIntiatedLSPList().add(pcepIntiatedLSPList);

        EndPointsIPv4 endP_IP = new EndPointsIPv4();
        endP_IP.setSourceIP(source_router_id_addr);
        endP_IP.setDestIP(dest_router_id_addr);

        pcepIntiatedLSPList.setEndPoint(endP_IP);

        log.info("BorderSessionList" + PCEManagementSession.borderSessions.size());
        log.info("BorderSessionList" + PCEManagementSession.oneSession.size());
        log.info("Border " + PCEManagementSession.borderSessions.get(1).shortInfo());
        Iterator<MultiPCESession> it_mdomain = PCEManagementSession.borderSessions.iterator();
        while(it_mdomain.hasNext()){
            MultiPCESession tmp  = it_mdomain.next();
            log.info("PCEID rmeote" + ((InetSocketAddress)tmp.getSocket().getRemoteSocketAddress()).getAddress());
            if( ( (InetSocketAddress)tmp.getSocket().getRemoteSocketAddress()).getAddress().toString().equals(source_router_id_addr)){
                log.info("FOUND Stiching HOST sending initiate");
                tmp.sendPCEPMessage(pceInit);
            }
        }
        return m_resp;
    }


    @Override
    public AlgorithmReservation getReserv() {
        return null;
    }
}
