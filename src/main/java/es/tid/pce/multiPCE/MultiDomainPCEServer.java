package es.tid.pce.multiPCE;

/**
 * PCE Domain Server.
 * <p>
 * It is the main class of a PCE that is responsible of a domain.
 * <p>
 * By default listens on port 4189
 *
 * @author Oscar, Eduardo
 */

import es.tid.pce.computingEngine.ReportDispatcher;
import es.tid.pce.computingEngine.RequestDispatcher;
import es.tid.pce.computingEngine.algorithms.ComputingAlgorithmManager;
import es.tid.pce.computingEngine.algorithms.ComputingAlgorithmManagerSSON;
import es.tid.pce.computingEngine.algorithms.ComputingAlgorithmPreComputation;
import es.tid.pce.computingEngine.algorithms.ComputingAlgorithmPreComputationSSON;
import es.tid.pce.computingEngine.algorithms.multiLayer.OperationsCounter;
import es.tid.pce.pcepsession.PCEPSessionsInformation;
import es.tid.pce.server.*;
import es.tid.pce.server.lspdb.ReportDB_Handler;
import es.tid.pce.server.lspdb.SingleDomainLSPDB;
import es.tid.pce.server.management.PCEManagementSever;
import es.tid.pce.server.wson.ReservationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Timer;

import java.net.*;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

import es.tid.tedb.*;

public class MultiDomainPCEServer implements Runnable {

    /**
     * Logger
     */
    public static final Logger log = LoggerFactory.getLogger("PCEServer");
    public static Logger log5;
    private static OperationsCounter OPcounter;

    private static ReportDB_Handler rptdb;
    private static boolean listening;

    /**
     * Parameters of the PCE
     */
    PCEServerParameters params;

    /**
     * Socket where the PCE is listening;
     */
    ServerSocket serverSocket;
    /**
     * Management server of PCE
     */
    PCEManagementSever pms;


    public void configure(String configFile) {
        if (configFile != null) {
            params = new PCEServerParameters(configFile);
        } else {
            params = new PCEServerParameters();
        }
        try {
            params.initialize();

            Logger log2 = LoggerFactory.getLogger("PCEPParser");
            Logger log3 = LoggerFactory.getLogger("OSPFParser");
            Logger log4 = LoggerFactory.getLogger("TEDBParser");
            Logger log6 = LoggerFactory.getLogger("BGPParser");
            Logger log5 = LoggerFactory.getLogger("OpMultiLayer");


        } catch (Exception e1) {
            e1.printStackTrace();
            System.exit(1);
        }

        log.info("Configuration file: " + configFile);
        log.info("Inizializing TID PCE Server!!");

    }

    public void run() {


        //Elements of the PCE Server

        // Information about all the sessions of the PCE
        PCEPSessionsInformation pcepSessionsInformation = new PCEPSessionsInformation();
        pcepSessionsInformation.setStateful(params.isStateful());
        pcepSessionsInformation.setStatefulDFlag(params.isStatefulDFlag());
        pcepSessionsInformation.setStatefulSFlag(params.isStatefulSFlag());
        pcepSessionsInformation.setStatefulTFlag(params.isStatefulTFlag());
        pcepSessionsInformation.setStatefulIFlag(params.isInitiate());


        pcepSessionsInformation.setActive(params.isActive());
        pcepSessionsInformation.setSRCapable(params.isSRCapable());
        pcepSessionsInformation.setMSD(params.getMSD());

        SingleDomainInitiateDispatcher iniDispatcher = null;

        SingleDomainLSPDB singleDomainLSPDB = null;
        IniPCCManager iniManager = null;

        if (params.isSRCapable())
            log.info("PCEServer: PCE is SR capable with MSD=" + pcepSessionsInformation.getMSD());

        //The Traffic Engineering Database
        DomainTEDB ted;
        //GENERIC PCE
        log.info("GENERIC PCE");
        ted = new VSPTSimpleTEDB();

        if (params.isStateful()) {
            log.info("Stateful PCE with T=" + params.isStatefulTFlag() + " D=" + params.isStatefulDFlag() + " S=" + params.isStatefulSFlag());
            singleDomainLSPDB = new SingleDomainLSPDB();
            if (params.getDbType().equals("_")) {
                singleDomainLSPDB.setExportDb(false);
            }
            iniManager = new IniPCCManager();
            iniDispatcher = new SingleDomainInitiateDispatcher(singleDomainLSPDB, iniManager);
        }


        /***/

        TopologyManager topologyManager = new TopologyManager(params, ted, log);
        topologyManager.initTopology();

        log.info("Reachability info:");
        log.info(( (VSPTSimpleTEDB) ted).getReachabilityManager().printReachability());

        OPcounter = new OperationsCounter();

        BorderPCERequestManager borderPCERequestManager = new BorderPCERequestManager();

        //The Request Dispatcher. Incoming Requests are sent here
        //RequestQueue pathRequestsQueue;
        RequestDispatcher requestDispatcher;
        RequestDispatcher localRequestDispatcher;

        log.info("Inizializing " + 1 + " Path Request Processor Threads");
        //pathRequestsQueue=new RequestQueue(params.getChildPCERequestsProcessors());

        // We need two request Dispatchers, one for our local compute and one for the "remote" compute.
        // All messages will arive at the "remote" compute side, if it is desicded that this is an internal
        // domain only we will punt it downwards to the "local" one.
        requestDispatcher = new RequestDispatcher(1, ted, null, false);
        localRequestDispatcher = new RequestDispatcher(1, ted, null, false);
        log.info("Inizializing " + 1 + " Ini Dispatcher");

        if (params.algorithmRuleList.size() == 0) {

            log.warn("No hay algoritmos registrados!");
            //System.exit(1);

        }
        //FIXME: cambiar esto de orden
        RequestDispatcher PCCRequestDispatcher = null;
        ReservationManager reservationManager = null;

        pms = new PCEManagementSever(PCCRequestDispatcher, ted, params, reservationManager, null);
        pms.start();

        SendTopologyTask stg = null;
        ITSendTopologyTask ITstg = null;
        RequestDispatcher PCCRequestDispatcherChild = null;


        listening = true;
        try {
            log.info("Listening on port: " + params.getPCEServerPort());

            // Local PCE address for multiple network interfaces in a single computer

            log.info("Listening on address: " + params.getLocalPceAddress());
            serverSocket = new ServerSocket(params.getPCEServerPort(), 0, (Inet4Address) InetAddress.getByName(params.getLocalPceAddress()));
        } catch (IOException e) {
            System.err.println("Could not listen on port: " + params.getPCEServerPort());
            System.exit(-1);
        }

        try {
            //Registration of Algorithms
            for (int i = 0; i < params.algorithmRuleList.size(); ++i) {
                try {
                    Class<?> aClass = Class.forName("es.tid.pce.computingEngine.algorithms." + params.algorithmRuleList.get(i).algoName + "Manager");
                    log.info("Registering algorithm " + params.algorithmRuleList.get(i).algoName + " for of = " + params.algorithmRuleList.get(i).ar.of + " and svec = " + params.algorithmRuleList.get(i).ar.svec);
                    if (params.algorithmRuleList.get(i).isParentPCEAlgorithm == false) {
                        if (params.algorithmRuleList.get(i).isSSSONAlgorithm == false) {
                            ComputingAlgorithmManager cam = (ComputingAlgorithmManager) aClass.newInstance();
                            PCCRequestDispatcher.registerAlgorithm(params.algorithmRuleList.get(i).ar, cam);
                            if ((params.getParentPCEAddress() != null)) {
                                PCCRequestDispatcherChild.registerAlgorithm(params.algorithmRuleList.get(i).ar, cam);
                            }
                            try {

                                Class<?> aClass2 = Class.forName("es.tid.pce.computingEngine.algorithms." + params.algorithmRuleList.get(i).algoName + "PreComputation");
                                ComputingAlgorithmPreComputation cam2 = (ComputingAlgorithmPreComputation) aClass2.newInstance();
                                cam2.setTEDB(ted);
                                cam2.initialize();
                                cam.setPreComputation(cam2);
                                ((DomainTEDB) ted).register(cam2);
                                cam.setReservationManager(reservationManager);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                log.warn("No precomputation in " + "es.tid.pce.computingEngine.algorithms." + params.algorithmRuleList.get(i).algoName + "PreComputation");
                            }
                        } else {
                            ComputingAlgorithmManagerSSON cam_sson = (ComputingAlgorithmManagerSSON) aClass.newInstance();
                            PCCRequestDispatcher.registerAlgorithmSSON(params.algorithmRuleList.get(i).ar, cam_sson);
                            if ((params.getParentPCEAddress() != null)) {
                                PCCRequestDispatcherChild.registerAlgorithmSSON(params.algorithmRuleList.get(i).ar, cam_sson);

                            }
                            try {
                                Class<?> aClass2 = Class.forName("es.tid.pce.computingEngine.algorithms." + params.algorithmRuleList.get(i).algoName + "PreComputation");
                                ComputingAlgorithmPreComputationSSON cam3 = (ComputingAlgorithmPreComputationSSON) aClass2.newInstance();
                                cam3.setTEDB(ted);
                                cam3.initialize();
                                cam_sson.setPreComputation(cam3);
                                ((DomainTEDB) ted).registerSSON(cam3);
                                cam_sson.setReservationManager(reservationManager);
                            } catch (Exception e2) {
                                e2.printStackTrace();
                                log.warn("No precomputation in " + "es.tid.pce.computingEngine.algorithms." + params.algorithmRuleList.get(i).algoName + "PreComputation");
                            }
                        }
                    }

                    //No registro los algoritmos que sean de parentPCE
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if ((params.getLambdaEnd() != Integer.MAX_VALUE) && (params.ITcapable == false) && ((params.isMultilayer()) == false))
                ((SimpleTEDB) ted).notifyAlgorithms(params.getLambdaIni(), params.getLambdaEnd());


            // This parameter tells the dispatcher that sync will be avoided.
            // In better future times sync should be implemented

            ReportDispatcher PCCReportDispatcher = null;
            //
            if (pcepSessionsInformation.isStateful()) {
                log.info("redis: " + params.getDbType() + " " + params.getDbName());
                rptdb = new ReportDB_Handler();

                params.setLspDB(rptdb);
                log.info("Creando dispatchers para el LSP DB");
                PCCReportDispatcher = new ReportDispatcher(rptdb, 2);
            }

            NotificationDispatcher nd = new NotificationDispatcher(null);


            //INIT multiPCESessions with border nodes
            LinkedList<String> borderServers =  ((VSPTSimpleTEDB) ted).getPceServer();

            Iterator<String> it = borderServers.iterator();
            LinkedList<MultiPCESessionManager> pcmList = new LinkedList<MultiPCESessionManager>();


            RequestDispatcher ParentRequestDispatcherBorder = null;
            ParentRequestDispatcherBorder = new RequestDispatcher(1, ted, null, params.isAnalyzeRequestTime());

            while(it.hasNext()){
                String serverName = it.next();
                log.info("Connecting to: " + serverName);
                try {
                    URL aURL = new URL("http://" +  serverName);
                    log.info("Host part:" +  InetAddress.getByName(aURL.getHost()));
                    log.info("Host part:" + aURL.getPort());
                    PCEServerParameters tmp =  new PCEServerParameters();
                    tmp.setParentPCEPort(aURL.getPort());
                    tmp.setParentPCEAddress(aURL.getHost());
                    tmp.setLocalPceAddress(params.getLocalPceAddress());
                    MultiPCESessionManager tmpSessionManager = new MultiPCESessionManager(ParentRequestDispatcherBorder, tmp, ted,
                            ((VSPTSimpleTEDB) ted).getReachabilityManager().getDomainForPCE(serverName), pcepSessionsInformation, iniDispatcher);
                    tmpSessionManager.childPCERequestManager.locks = borderPCERequestManager.locks;
                     pcmList.add(tmpSessionManager);


                }catch (IOException e){
                    log.error(e.getMessage());
                }
            }
            //start border sessions...
            Iterator<MultiPCESessionManager> it2 = pcmList.iterator();
            while(it2.hasNext()) {
                Timer tmp_timer= new Timer();
                tmp_timer.schedule(it2.next(), 0, 100000);
            }



                // WHile do normal PCE computation
            PCCRequestDispatcher = new RequestDispatcher(params.getPCCRequestsProcessors(), ted, pcmList.get(0).getChildPCERequestManager(), params.isAnalyzeRequestTime());

            while (listening) {
                //new PCESession(serverSocket.accept(),params, PCCRequestsQueue,ted,pcm.getChildPCERequestManager()).start();
                //null,ted,pcm.getChildPCERequestManager()).start(
                //new DomainPCESession(serverSocket.accept(), params, PCCRequestDispatcher, ted, nd, reservationManager, pcepSessionsInformation, PCCReportDispatcher, iniDispatcher).start();
                new MultiPCESession(serverSocket.accept(), params, PCCRequestDispatcher,null, ted, null, borderPCERequestManager, null, pcepSessionsInformation).start();
                log.info("Got a connection");
            }
            serverSocket.close();

        } catch (SocketException e) {
            if (listening == false) {
                log.info("Socket closed due to controlled close");
            } else {
                log.error("Problem with the socket, exiting");
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void stopServer() {
        pms.stopServer();
        listening = false;
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

}
